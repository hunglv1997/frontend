import { toast } from 'react-toastify';

export const formatCurrency = (value) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  return formatter.format(value);
};


/**
 *
 * @param message
 * @param type: error or success
 */
export const toastMessage = async (message, type) => {
  if (type === 'error') {
    if (Array.isArray(message)) {
      for (const mss of message) {
        toastError(mss);
      }
    } else {
      toastError(message);
    }
  } else if (type === 'success') {
    toastSuccess(message);
    // Wait for the notification to be displayed before redirecting
    await new Promise((resolve) => setTimeout(resolve, 1000));
  }
};


export const toastSuccess = (content) => {
  toast.success(content);
};

export const toastError = (content) => {
  toast.error(content);
};