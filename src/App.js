import './App.css';
import ProductList from "./components/Product/ProductList";
import CustomerList from "./components/customer/CustomerList";
import {Col, Row} from "antd";

function App() {

  return (
    <div style={{margin: '20px'}}>
        <Row>
            <Col lg={{
                span: 6,
                offset: 2,
            }}>
                <CustomerList  />
            </Col>
            <Col lg={{
                span: 10,
                offset: 2,
            }}>
                <ProductList />
            </Col>
        </Row>
    </div>
  );
}

export default App;
