import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function MyToastContainer() {
  return <ToastContainer theme="colored" />;
}
