import {Button, Pagination, Table} from "antd";
import {useEffect, useState} from "react";
import {getListProductCustomerShop} from "../../services/product";
import {formatCurrency} from "../../utils/common";
import CustomerAdd from "../customer/CustomerAdd";

const columns = [
    {
        title: 'Customer',
        dataIndex: 'customer',
        key: 'customer',
    },
    {
        title: 'Shop',
        dataIndex: 'shop',
        key: 'shop',
    },
    {
        title: 'Product',
        dataIndex: 'product',
        key: 'product',
    },
];


function ProductList() {
    const [items, setItems] = useState([]);
    const [totalRows, setTotalRows] = useState(0);
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(10);

    useEffect( () => {
         getListProduct().then();
    }, []);

    useEffect(  () => {
        getListProduct().then();
    }, [page, pageSize]);

    const getListProduct= async () => {
        try {
            const response = await getListProductCustomerShop(page, pageSize);
            console.log(response);
            setItems(response.items.map((item, key) =>  {
               return {
                   key,
                   customer: `${item.customerFullName} - ${item.customerEmail}`,
                   shop: `${item.shopName} - ${item.shopLocation}`,
                   product: `${item.productName} - ${item.productImage} - ${ formatCurrency(item.productPrice)}`,
               }
            }));
            setTotalRows(response.totalRows);
        } catch (e) {
            console.log(e);
        }
    };

    const onChangePage = async (page) => {
        setPage(page)
    }

    const onShowSizeChange = async (current, pageSize) => {
         setPageSize(pageSize)
    }

    return (
        <div style={{marginTop: "30px"}}>
            <div style={{margin: "5px"}}> <strong> Danh sách Product </strong> </div>
            <Table dataSource={items} columns={columns} pagination={false} bordered />
            <div style={{marginTop: "10px", display: "flex", justifyContent: "center"}}>
                <Pagination defaultCurrent={1} current={page} total={totalRows}
                            onChange={onChangePage}
                            defaultPageSize={pageSize}
                            onShowSizeChange={onShowSizeChange}
                />
            </div>
        </div>
    )
}

export default ProductList;
