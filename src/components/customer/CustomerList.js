import {Table} from "antd";
import {useEffect, useState} from "react";
import {getListProductCustomerShop} from "../../services/product";
import {formatCurrency} from "../../utils/common";
import {getAllCustomers} from "../../services/customer";
import CustomerAdd from "./CustomerAdd";


const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
];


const CustomerList = () => {
    const [items, setItems] = useState([]);

    useEffect( () => {
        getListCustomer().then();
    }, []);

    const getListCustomer= async () => {
        try {
            const response = await getAllCustomers();
            console.log(response);
            setItems(response.map((item, key) =>  {
                return {
                    key,
                    name: `${item.fullName}`,
                    email: `${item.email}`,
                }
            }));
        } catch (e) {
            console.log(e);
        }
    };

    const loadListCustomerAgain = async () => {
        await getListCustomer();
    }

    return(
        <div>
            <CustomerAdd loadListCustomerAgain={loadListCustomerAgain} />
             <div style={{margin: "5px"}}> <strong> Danh sách Customer </strong> </div>
            <Table dataSource={items} columns={columns} pagination={false}  bordered />
        </div>
    )
}

export default CustomerList;