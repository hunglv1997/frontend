import React, {useEffect, useState} from 'react';
import {Button, DatePicker, Form, Input, Modal} from 'antd';
import {addCustomer} from "../../services/customer";
import MyToastContainer from "../common/MyToastContainer";
import {toastMessage} from "../../utils/common";
const CustomerAdd = (props) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [inputValue, setInputValue] = useState({
        fullName: "",
        dateOfBirth : "",
        email: ""
    });

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = async () => {
        try {
            const response =  await addCustomer(inputValue);
            console.log(response);
            setIsModalOpen(false);
            await toastMessage("Add successfully", 'success');
            props.loadListCustomerAgain();
        } catch (e) {
            console.log(e);
        }
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const onChangeDateOfBirth = (value, dateString) => {
        const inputValueTemp = { ...inputValue };
        inputValueTemp.dateOfBirth = dateString;
        setInputValue(inputValueTemp);
    };

    const onChangeInput = (e) => {
        const value = e.target.value;
        const name = e.target.name;
        const inputValueTemp = { ...inputValue };
        inputValueTemp[name] = value;
        setInputValue(inputValueTemp);
    }

    return (
        <>
            <Button type="primary" onClick={showModal}>
                Add Customer
            </Button>
            <Modal title="Add Customer" open={isModalOpen} onOk={handleOk}  onCancel={handleCancel}>
                <Form
                    name="wrap"
                    labelCol={{
                        flex: '110px',
                    }}
                    labelAlign="left"
                    labelWrap
                    wrapperCol={{
                        flex: 1,
                    }}
                    colon={false}
                    style={{
                        maxWidth: 600,
                    }}
                >
                    <Form.Item
                        label="Full Name"
                        name="fullName"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input name={"fullName"} value={inputValue.fullName} onChange={onChangeInput} />
                    </Form.Item>

                    <Form.Item
                        label="Date Of Birth"
                        name="dateOfBirth"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <DatePicker showTime onChange={onChangeDateOfBirth}  />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input name={"email"} value={inputValue.email} onChange={onChangeInput} />
                    </Form.Item>
                </Form>
            </Modal>

            <MyToastContainer />
        </>
    );
};
export default CustomerAdd;