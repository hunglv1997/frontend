import {makeRequest} from "./api";
import {API_URL} from "../utils/constants";

export async function getListProductCustomerShop(pageIndex, pageSize) {
    return await makeRequest({
        method: 'get',
        url: `${API_URL}/api/product/list?pageIndex=${pageIndex}&pageSize=${pageSize}`,
    });
}