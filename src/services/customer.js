import {makeRequest} from "./api";
import {API_URL} from "../utils/constants";

export async function addCustomer(data) {
    return await makeRequest({
        method: 'post',
        url: `${API_URL}/api/customer/add`,
        data,
    });
}

export async function getAllCustomers() {
    return await makeRequest({
        method: 'get',
        url: `${API_URL}/api/customer/all`,
    });
}