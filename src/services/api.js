import axios from 'axios';

function DataException(data) {
  this.data = data;
  this.name = 'Exception';
}

export const makeRequest = async ({
  method,
  url,
  data = null,
  headers = { 'Content-Type': 'application/json' },
  withCredentials = false,
}) => {
  try {
    let options = {
      method,
      url,
      headers: {
        ...headers,
      },
      withCredentials,
    };
    if (method !== 'delete') {
      options = { ...options, data };
    }

    const response = await axios(options);
    return response.data;
  } catch (e) {
    if (e.response.data.statusCode === 401) {
        console.log("Unauthenticated")
    } else {
      throw new DataException(e.response.data);
    }
  }
};
